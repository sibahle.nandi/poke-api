package za.co.standardbank.pokeapi.dto;

import lombok.Data;

import java.util.List;

@Data
public class PokemonDetailDto {

    private List<PokemonResult> results;

    public PokemonDetailDto() {
    }
}
