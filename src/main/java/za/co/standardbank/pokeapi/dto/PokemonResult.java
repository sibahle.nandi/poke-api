package za.co.standardbank.pokeapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class PokemonResult {
    private String name;
    private String url;

    public PokemonResult() {
    }
}
