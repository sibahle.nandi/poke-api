package za.co.standardbank.pokeapi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.standardbank.pokeapi.dto.PokemonDetailDto;
import za.co.standardbank.pokeapi.dto.PokemonResult;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PokemonServer implements PokemonService {
    private static final Logger logger = LoggerFactory.getLogger(PokemonServer.class);
    @Autowired
    private PokemonRestApiService pokemonRestApiService;


    @Override
    public List<PokemonResult> getListOfPokemon() {
        PokemonDetailDto pokemonDetailDto = retrivePokemonsFromApi();
        return pokemonDetailDto.getResults();
    }

    private PokemonDetailDto retrivePokemonsFromApi() {
        PokemonDetailDto allPokemons = pokemonRestApiService.getAllPokemons();
        logger.info("Retrieved pokemon details : " + allPokemons);
        return allPokemons;
    }

    @Override
    public List<PokemonResult> getPokemonByName(String pokemonName, String... pokemonNames) {
        PokemonDetailDto pokemonDetailDto = retrivePokemonsFromApi();
        List<PokemonResult> pokemonResultList = pokemonDetailDto.getResults();
        Set<PokemonResult> results = new HashSet<>();
        addToResults(pokemonResultList, results, pokemonName);
        int counter = 0;
        while (pokemonNames != null && counter < pokemonNames.length) {
            String searchByPokemonName = pokemonNames[counter++];
            addToResults(pokemonResultList, results, searchByPokemonName);
        }
        return results.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    private void addToResults(List<PokemonResult> pokemonResultList, Set<PokemonResult> results, String searchByPokemonName) {
        results.add(filterByName(searchByPokemonName, pokemonResultList));
    }

    private PokemonResult filterByName(String pokemonName, List<PokemonResult> pokemonResultList) {
        for (PokemonResult pokemonResult : pokemonResultList) {
            if (pokemonName.equals(pokemonResult.getName())) {
                return pokemonResult;
            }
        }
        return null;
    }
}
