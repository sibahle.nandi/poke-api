package za.co.standardbank.pokeapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import za.co.standardbank.pokeapi.dto.PokemonDetailDto;

@Service
public class PokemonRestApiService {
    @Autowired
    private WebClient webClient;
    @Value("${third.party.api.uri}")
    private String pokemonUri;

    public PokemonDetailDto getAllPokemons() {

        return webClient.get().uri(pokemonUri)
                .retrieve()
                .bodyToMono(PokemonDetailDto.class)
                .block();
    }
}
