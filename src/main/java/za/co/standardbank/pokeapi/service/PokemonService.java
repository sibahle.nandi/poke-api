package za.co.standardbank.pokeapi.service;

import javafx.util.Pair;
import za.co.standardbank.pokeapi.dto.PokemonResult;

import java.util.List;

public interface PokemonService {
    List<PokemonResult> getListOfPokemon();

    List<PokemonResult> getPokemonByName(String pokemon, String... pokemonNames);
}
