package za.co.standardbank.pokeapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class PokemonConfiguration {
    @Bean
    public WebClient createWebClient(@Value("${third.party.api.url}") String baseUrl) {
        return WebClient.create(baseUrl);
    }
}
