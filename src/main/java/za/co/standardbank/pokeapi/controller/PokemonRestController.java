package za.co.standardbank.pokeapi.controller;


import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import za.co.standardbank.pokeapi.dto.PokemonResult;
import za.co.standardbank.pokeapi.service.PokemonService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PokemonRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PokemonRestController.class);
    public static final String API_LIST_POKEMON = "/api/list/pokemon";
    public static final String API_SEARCH_POKEMON = "/api/search/pokemon";
    public static final String RETRIEVING_LIST_OF_POKEMONS_CONTACT_ADMIN_ERROR_MESSAGE = "Error while retrieving list of pokemons , contact Admin BLA BLA BLA";
    public static final String SEARCHING_FOR_POKEMON_CONTACT_ADMIN_ERROR_MESSAGE = "Error while searching for pokemon, contact Admin BLA BLA BLA";
    @Autowired
    private PokemonService pokemonService;

    @GetMapping(API_LIST_POKEMON)
    public @ResponseBody
    List<PokemonResult> listPokemon() {
        try {
            return pokemonService.getListOfPokemon();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, RETRIEVING_LIST_OF_POKEMONS_CONTACT_ADMIN_ERROR_MESSAGE);
        }
    }

    @GetMapping(API_SEARCH_POKEMON)
    public @ResponseBody
    List<PokemonResult> findPokemonByName(@RequestParam(name = "pokemonName") String pokemonName,
                                          @RequestParam(name = "pokemonNames", required = false) String... pokemonNames) {
        try {
            return pokemonService.getPokemonByName(pokemonName, pokemonNames);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, SEARCHING_FOR_POKEMON_CONTACT_ADMIN_ERROR_MESSAGE);
        }
    }


    /*
     * TODO : Method has no logic, I was not sure of the requirement. Which details are we looking for from the third party call
     *  Couldn't differentiate between the search functionality and getting the details.
     * */

    @GetMapping("/api/pokemon/detail")
    public @ResponseBody
    List<String> getPokemonDetails(@RequestParam(name = "pokemonName") String pokemonName) {
        throw new UnsupportedOperationException("Not supported yet, Please update requirements");
    }
}
