package za.co.standardbank.pokeapi.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.FileCopyUtils;
import za.co.standardbank.pokeapi.dto.PokemonDetailDto;
import za.co.standardbank.pokeapi.dto.PokemonResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PokemonServerTest {
    @InjectMocks
    private PokemonServer pokemonServer;
    @Mock
    private PokemonRestApiService pokemonRestApiService;
    private PokemonDetailDto pokemonDetails;

    @BeforeAll
    void resourceIntensiveSetUp() throws IOException {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("static/pokeapi.co.json");
        byte[] bytes = FileCopyUtils.copyToByteArray(resourceAsStream);
        String allPokemonJsonString = new String(bytes);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        pokemonDetails = objectMapper.readValue(allPokemonJsonString, new TypeReference<PokemonDetailDto>() {
        });
        System.out.println(pokemonDetails);
    }

    @BeforeEach
    void setUP() {
        when(pokemonRestApiService.getAllPokemons()).thenReturn(pokemonDetails);
    }

    @Test
    void getListOfPokemonsFromTheApi() {
        pokemonServer.getListOfPokemon();

        verify(pokemonRestApiService).getAllPokemons();
    }

    @Test
    void returnJsonStringWhenCallingRestApi() {

        List<PokemonResult> listOfPokemon = pokemonServer.getListOfPokemon();

        verify(pokemonRestApiService).getAllPokemons();

        assertNotNull(listOfPokemon);
        assertAll("Pokemon Results",
                () -> assertTrue(listOfPokemon.contains(new PokemonResult("charmander", "https://pokeapi.co/api/v2/pokemon/4/"))),
                () -> assertTrue(listOfPokemon.containsAll(pokemonDetails.getResults())));
    }

    @Test
    void searchPokemonByNameOnly() {
        Random random = new Random();
        int randomElement = random.nextInt(pokemonDetails.getResults().size());
        PokemonResult pokemonResult = pokemonDetails.getResults().get(randomElement);
        List<PokemonResult> pokemonResults = pokemonServer.getPokemonByName(pokemonResult.getName());

        verify(pokemonRestApiService).getAllPokemons();

        assertEquals(pokemonResults.size(), 1);
        assertTrue(pokemonResults.contains(pokemonResult));
    }

    @Test
    void searchPokemonInVarargNames() {
        Random random = new Random();
        int randomElement1 = random.nextInt(pokemonDetails.getResults().size());
        int randomElement2 = random.nextInt(pokemonDetails.getResults().size());
        int randomElement3 = random.nextInt(pokemonDetails.getResults().size());
        PokemonResult pokemonResult1 = pokemonDetails.getResults().get(randomElement1);
        PokemonResult pokemonResult2 = pokemonDetails.getResults().get(randomElement2);
        PokemonResult pokemonResult3 = pokemonDetails.getResults().get(randomElement3);
        List<PokemonResult> pokemonResults = pokemonServer.getPokemonByName(pokemonResult1.getName(), pokemonResult2.getName(), pokemonResult3.getName());

        verify(pokemonRestApiService).getAllPokemons();

        assertEquals(3, pokemonResults.size());
        assertAll("Searched Pokemons",
                () -> assertTrue(pokemonResults.contains(pokemonResult1)),
                () -> assertTrue(pokemonResults.contains(pokemonResult2)),
                () -> assertTrue(pokemonResults.contains(pokemonResult3)));
    }

    @Test
    void doNotReturnAListWithNullValuesWhenPokemonNameDoesNotExist() {
        Random random = new Random();
        int randomElement1 = random.nextInt(pokemonDetails.getResults().size());
        int randomElement2 = random.nextInt(pokemonDetails.getResults().size());

        PokemonResult pokemonResult1 = pokemonDetails.getResults().get(randomElement1);
        PokemonResult pokemonResult2 = pokemonDetails.getResults().get(randomElement2);
        List<PokemonResult> pokemonResults = pokemonServer.getPokemonByName(pokemonResult1.getName(), pokemonResult2.getName(), "Non existing pokemon name");

        verify(pokemonRestApiService).getAllPokemons();
        assertTrue(pokemonResults.stream().allMatch(Objects::nonNull));
        assertEquals(2, pokemonResults.size());
        assertAll("Searched Pokemon Results",
                () -> assertTrue(pokemonResults.contains(pokemonResult1)),
                () -> assertTrue(pokemonResults.contains(pokemonResult2)));
    }

    @Test
    void doNotReturnDuplicateValuesWhenSearchingForTheSameName() {
        Random random = new Random();
        int randomElement1 = random.nextInt(pokemonDetails.getResults().size());

        PokemonResult pokemonResult1 = pokemonDetails.getResults().get(randomElement1);

        List<PokemonResult> pokemonResults = pokemonServer.getPokemonByName(pokemonResult1.getName(), pokemonResult1.getName());

        verify(pokemonRestApiService).getAllPokemons();
        assertTrue(pokemonResults.stream().allMatch(Objects::nonNull));
        assertEquals(1, pokemonResults.size());
        assertTrue(pokemonResults.contains(pokemonResult1));
    }

    @Test
    void doNotUseNullVarArg() {
        Random random = new Random();
        int randomElement1 = random.nextInt(pokemonDetails.getResults().size());

        PokemonResult pokemonResult1 = pokemonDetails.getResults().get(randomElement1);

        List<PokemonResult> pokemonResults = pokemonServer.getPokemonByName(pokemonResult1.getName(), (String[]) null);

        verify(pokemonRestApiService).getAllPokemons();
        assertTrue(pokemonResults.stream().allMatch(Objects::nonNull));
        assertEquals(1, pokemonResults.size());
        assertTrue(pokemonResults.contains(pokemonResult1));
    }
}