package za.co.standardbank.pokeapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.util.Pair;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import za.co.standardbank.pokeapi.dto.PokemonResult;
import za.co.standardbank.pokeapi.service.PokemonService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PokemonRestController.class)
class PokemonRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PokemonService pokemonService;

    @BeforeEach
    void setUp(){
        List<PokemonResult> testList = new ArrayList<>();
        testList.add(new PokemonResult("pokemonName", "url1"));
        testList.add(new PokemonResult("pokemonName2", "pokemon Name2"));
        when(pokemonService.getPokemonByName(any(), any())).thenReturn(testList);
    }

    @Test
    void getListOfPokemonsEndPointReachable() throws Exception {
        mockMvc.perform(get("/api/list/pokemon").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void returnListFromBusinessService() throws Exception {
        List<PokemonResult> testList = new ArrayList<>();
        testList.add(new PokemonResult("pokemonName", "pokemonName url"));
        when(pokemonService.getListOfPokemon()).thenReturn(testList);

        mockMvc.perform(get("/api/list/pokemon").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("pokemonName")))
                .andExpect(jsonPath("$[0].url", Matchers.is("pokemonName url")))
                .andDo(print());

        verify(pokemonService).getListOfPokemon();
    }

    @Test
    void returnListOfPokemonsWithNames() throws Exception {



        mockMvc.perform(get("/api/search/pokemon").contentType(MediaType.APPLICATION_JSON)
                .param("pokemonName", "Chris"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", Matchers.is("pokemonName")))
                .andDo(print());

        verify(pokemonService).getPokemonByName(eq("Chris"), nullable(String[].class));
    }

    @Test
    @Disabled("Need to check why arguments capture fails on Class cast exception(Suspect version after anyVararg() is deprecated)")
    void varArgsArePassedAsParametersToTheService() throws Exception {


        ArgumentCaptor<String[]> searchNameCapture = ArgumentCaptor.forClass(String[].class);

        mockMvc.perform(get("/api/search/pokemon").contentType(MediaType.APPLICATION_JSON)
                .param("pokemonName", "PokemonName Olu")
                .param("pokemonNames", "Chris", "Sibahle"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", Matchers.is("pokemonName")))
                .andDo(print());

        verify(pokemonService).getPokemonByName(anyString(), searchNameCapture.capture());

        String[] nameArgumentsFromRequest = searchNameCapture.getValue();
        assertAll("Passed names to search with",
                () -> assertEquals("Chris", nameArgumentsFromRequest[0]),
                () -> assertEquals("Sibahle", nameArgumentsFromRequest[1]));
    }

    @Test
    void searchUsingVarArgsFromRequest() throws Exception {


        mockMvc.perform(get("/api/search/pokemon").contentType(MediaType.APPLICATION_JSON)
                .param("pokemonName", "PokemonName Olu")
                .param("pokemonNames", "Chris", "Sibahle"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", Matchers.is("pokemonName")))
                .andDo(print());

        verify(pokemonService).getPokemonByName(eq("PokemonName Olu"), any());
    }

    @Test
    void rethrowExceptionWhenExperiencingException() throws Exception {
        when(pokemonService.getListOfPokemon()).thenThrow(new RuntimeException("Runtime exception during a list call"));

        ResultActions resultActions = mockMvc.perform(get("/api/list/pokemon").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();

        String errorMessage = response.getErrorMessage();

        verify(pokemonService).getListOfPokemon();
        assertEquals(errorMessage, "Error while retrieving list of pokemons , contact Admin BLA BLA BLA");
    }

    @Test
    void rethrowExceptionWhenSearchingForPokemon() throws Exception {
        when(pokemonService.getPokemonByName(any(), any())).thenThrow(new RuntimeException("Runtime exception during a search"));

        ResultActions resultActions = mockMvc.perform(get("/api/search/pokemon").contentType(MediaType.APPLICATION_JSON)

                .param("pokemonName", "Test pokemon name"))
                .andExpect(status().isInternalServerError());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();

        String errorMessage = response.getErrorMessage();

        verify(pokemonService).getPokemonByName(any(), any());

        assertEquals(errorMessage, "Error while searching for pokemon, contact Admin BLA BLA BLA");
    }
}